const moment = require("moment");
const formatters = require("../../lib/formatters");
const ediHelper = require("../../lib/edi-helper");

exports.options = {
  logoURL: "https://tsvplatform.blob.core.windows.net/banks/itau.jpeg",
  codigo: "341",
};

//Gera o digito do Codigo de Barras - Anexo 2 da documentacao Itau API 1.0
exports.dvBarra = function (barra) {
  let resto = formatters.mod11(barra, 9, 1); //Aqui o resto = 1 é para retornar (soma % 11)
  resto = 11 - resto;
  return resto == 0 || resto == 1 || resto == 10 || resto == 11
    ? 1
    : resto;
};

exports.barcodeData = function (boleto) {
  const codigoBanco = this.options.codigo;
  const numMoeda = "9";
  const fatorVencimento = formatters.fatorVencimento(boleto["data_vencimento"]);
  const valor = formatters.addTrailingZeros(boleto["valor"], 10);
  const carteira = boleto["carteira"];
  const nossoNumero = formatters.addTrailingZeros(boleto["nosso_numero"], 8);
  const agencia = formatters.addTrailingZeros(boleto["agencia"], 4);
  const conta = formatters.addTrailingZeros(boleto["codigo_cedente"], 5);
  boleto["nosso_numero_dv"] = formatters.mod10(
    [agencia, conta, carteira, nossoNumero].join("")
  );

  const barra =
    codigoBanco +
    numMoeda +
    fatorVencimento +
    valor +
    carteira +
    nossoNumero +
    formatters.mod10(agencia + conta + carteira + nossoNumero) +
    agencia +
    conta +
    formatters.mod10(agencia + conta) +
    "000";
  const dvBarra = this.dvBarra(barra);
  const lineData =
    barra.substring(0, 4) + dvBarra + barra.substring(4, barra.length);

  boleto["codigo_cedente"] = [
    conta,
    "-",
    formatters.mod10([agencia, conta].join("")),
  ].join("");
  boleto["nosso_numero"] = carteira + "/" + nossoNumero;

  // var codigoCedente = formatters.addTrailingZeros(boleto['codigo_cedente'], 7)
  // var nossoNumero = carteira + formatters.addTrailingZeros(boleto['nosso_numero'], 11)

  return lineData;
};

exports.linhaDigitavel = function (barcodeData) {
  //Conteudo de barcodeData, seguindo Itau API 1.0
  // 01-03    -> Código do banco sem o digito
  // 04-04    -> Código da Moeda (9-Real)
  // 05-05    -> Dígito verificador do código de barras
  // 06-09    -> Fator de vencimento
  // 10-19    -> Valor Nominal do Título
  // 20-22    -> carteira
  // 23-30    -> nosso numero
  // 31-31    -> DAC (Agencia/Conta/Carteira/Nosso Numero)
  // 32 a 35  -> Numero Agencia Beneficiario
  // 36 a 40  -> Numero da Conta corrente
  // 41 a 41  -> DAC (Agencia/Conta Corrente)
  // 42 a 44  -> Zeros

  const campos = [];

  let campo =
    barcodeData.substr(0, 3) +
    barcodeData.substr(3, 1) +
    barcodeData.substr(19, 3) +
    barcodeData.substr(22, 2);
  campo = campo + formatters.mod10(campo);
  campo = campo.substr(0, 5) + "." + campo.substr(5);
  campos.push(campo);

  campo =
    barcodeData.substr(24, 6) +
    barcodeData.substr(30, 1) +
    barcodeData.substr(31, 3);
  campo = campo + formatters.mod10(campo);
  campo = campo.substr(0, 5) + "." + campo.substr(5);
  campos.push(campo);

  campo =
    barcodeData.substr(34, 1) +
    barcodeData.substr(35, 6) +
    barcodeData.substr(41, 3);
  campo = campo + formatters.mod10(campo);
  campo = campo.substr(0, 5) + "." + campo.substr(5);
  campos.push(campo);

  campo = barcodeData.substr(4, 1);
  campos.push(campo);

  campo = barcodeData.substr(5, 4) + barcodeData.substr(9, 10);
  campos.push(campo);

  return campos.join(" ");
};

exports.parseEDIFile = function (fileContent) {
  try {
    const lines = fileContent.split("\n");
    const parsedFile = {
      boletos: [],
    };

    for (let i = 0; i < lines.length; i++) {
      const line = lines[i];
      const registro = line.substring(0, 1);

      if (registro == "0") {
        parsedFile["razao_social"] = line.substring(46, 76);
        parsedFile["data_arquivo"] = ediHelper.dateFromEdiDate(
          line.substring(94, 100)
        );
      } else if (registro == "1") {
        const boleto = {};

        parsedFile["cnpj"] = formatters.removeTrailingZeros(
          line.substring(3, 17)
        );
        parsedFile["carteira"] = formatters.removeTrailingZeros(
          line.substring(22, 24)
        );
        parsedFile["agencia_cedente"] = formatters.removeTrailingZeros(
          line.substring(24, 29)
        );
        parsedFile["conta_cedente"] = formatters.removeTrailingZeros(
          line.substring(29, 37)
        );

        boleto["codigo_ocorrencia"] = line.substring(108, 110);

        const ocorrenciasStr = line.substring(318, 328);
        const motivosOcorrencia = [];
        let isPaid =
          parseInt(boleto["valor_pago"]) >= parseInt(boleto["valor"]) ||
          boleto["codigo_ocorrencia"] == "06";

        for (let j = 0; j < ocorrenciasStr.length; j += 2) {
          const ocorrencia = ocorrenciasStr.substr(j, 2);
          motivosOcorrencia.push(ocorrencia);

          if (ocorrencia != "00") {
            isPaid = false;
          }
        }

        boleto["motivos_ocorrencia"] = motivosOcorrencia;
        boleto["data_ocorrencia"] = ediHelper.dateFromEdiDate(
          line.substring(110, 116)
        );
        boleto["data_credito"] = ediHelper.dateFromEdiDate(
          line.substring(295, 301)
        );
        boleto["vencimento"] = ediHelper.dateFromEdiDate(
          line.substring(110, 116)
        );
        boleto["valor"] = formatters.removeTrailingZeros(
          line.substring(152, 165)
        );
        boleto["banco_recebedor"] = formatters.removeTrailingZeros(
          line.substring(165, 168)
        );
        boleto["agencia_recebedora"] = formatters.removeTrailingZeros(
          line.substring(168, 173)
        );
        boleto["paid"] = isPaid;
        boleto["edi_line_number"] = i;
        boleto["edi_line_checksum"] = ediHelper.calculateLineChecksum(line);
        boleto["edi_line_fingerprint"] =
          boleto["edi_line_number"] + ":" + boleto["edi_line_checksum"];
        boleto["nosso_numero"] = formatters.removeTrailingZeros(
          line.substring(70, 81)
        );

        boleto["juros_operacao_em_atraso"] = formatters.removeTrailingZeros(
          line.substring(201, 214)
        );
        boleto["iof_devido"] = formatters.removeTrailingZeros(
          line.substring(214, 227)
        );
        boleto["abatimento_concedido"] = formatters.removeTrailingZeros(
          line.substring(227, 240)
        );
        boleto["desconto_concedido"] = formatters.removeTrailingZeros(
          line.substring(240, 253)
        );
        boleto["valor_pago"] = formatters.removeTrailingZeros(
          line.substring(253, 266)
        );
        boleto["juros_mora"] = formatters.removeTrailingZeros(
          line.substring(266, 279)
        );
        boleto["outros_creditos"] = formatters.removeTrailingZeros(
          line.substring(279, 292)
        );

        parsedFile.boletos.push(boleto);
      }
    }

    return parsedFile;
  } catch (e) {
    console.log(e);
    return null;
  }
};
